// --------------- DEPENDENCIES --------------- //

const config               = require('config');
const yFinance             = require('yahoo-finance');

// --------------- CONSTANTS --------------- //

const DATE_QUERY_RANGE = config.get("earnings-straddle-calculator.date-query-range");

// --------------- FUNCTIONS --------------- //

function getEarningsData(_Req, _Res) {
  let symbol = _Req.query.symbol;
  let dateString = _Req.query.date;
  let index = _Req.query.index;
  console.log(`SYMBOL: ${symbol}`);
  console.log(`DATE STRING: ${dateString}`);
  
  let date = new Date(dateString);
  let preDate = new Date(dateString);
  let postDate = new Date(dateString);
  preDate.setDate(preDate.getDate() - DATE_QUERY_RANGE);
  postDate.setDate(postDate.getDate() + DATE_QUERY_RANGE);
  let preDateString = preDate.toISOString().slice(0, 10);
  let postDateString = postDate.toISOString().slice(0, 10);
  console.log(`PRE DATE STRING: ${preDateString}`);
  console.log(`POST DATE STRING: ${postDateString}`);
  
  yFinance.historical({
    symbol: symbol,
    from: preDateString,
    to: postDateString
  }, (err, quotes) => {
    if (err) { throw err; }
    console.log(`=== ${symbol} (${quotes.length}) ===`);
    if (quotes[0]) {
      console.log(
        '%s\n...\n%s',
        JSON.stringify(quotes[0], null, 2),
        JSON.stringify(quotes[quotes.length - 1], null, 2)
      );
      _Res.send({data: quotes, index: index});
    } else {
      console.log('N/A');
      _Res.send({data: null});
    }
  });
}

module.exports = {
  getEarningsData: getEarningsData
}