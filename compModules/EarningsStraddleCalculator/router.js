// --------------- DEPENDENCIS --------------- //

const express                      = require('express');
const EarningsStraddleCalculator   = require('./functions.js');

const router                       = express.Router();

// --------------- ROUTES --------------- //

router.get('/getEarningsData', (req, res) => {
  EarningsStraddleCalculator.getEarningsData(req, res);
});

module.exports = router;