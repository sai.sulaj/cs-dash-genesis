var HEX_GREEN = '#7cd991';
var HEX_RED = '#ea8e98';

if (!firebase.apps.length) {
  firebase.initalizeApp({});
}

var numAnomalies = 0;

$(document).ready(() => {
  initDataListeners();
});

function initDataListeners() {
  firebase.database().ref('priceChannelBot').on('child_added', (exchangeSnapshot) => {
    console.log('Exchange added.');
    exchangeSnapshot.ref.on('child_added', (symbolSnapshot) => {
      console.log(`Exchange snapshot: ${exchangeSnapshot.numChildren()}`);
      console.log(`Symbol snapshot: ${symbolSnapshot.numChildren()}`);
      let timestampMin = Date.now();
      console.log(timestampMin);
      symbolSnapshot.child('timeseries').ref.orderByChild('timestamp').startAt(timestampMin).on('child_added', (activeSnapshot) => {
        updatePriceChannelBotAnomalies(exchangeSnapshot, symbolSnapshot, activeSnapshot);
      })
    });
  });
}

function updatePriceChannelBotAnomalies(_ExchangeSnapshot, _SymbolSnapshot, _DataSnapshot) {
  let itemIDStr = `#${_ExchangeSnapshot.key}-${_SymbolSnapshot.key}`;
  let exists = $(itemIDStr).length == 1;
  
  let exchangeName = _ExchangeSnapshot.key;
  let symbolName = _SymbolSnapshot.key;
  let timestamp = _DataSnapshot.child('timestamp').val();
  let isActive = _DataSnapshot.child('active').val();
  
  if (isDataValid(exchangeName, symbolName, timestamp, isActive)) {
    
    let exchangeNameHumanReadable = exchangeName.toUpperCase().replace('_', '.');
    let symbolNameHumanReadable = symbolName.toUpperCase().replace('_', '/');
    
    let timeDiff = new Date() - timestamp;
    let hoursDiff = Math.floor((timeDiff % 86400000) / 3600000);
    let minsDiff = Math.round((((timeDiff - (hoursDiff * 3600000)) % 86400000) % 3600000) / 60000);
    
    if (exists) {
      console.log('EXISTS');
      if (isActive) {
        console.log('IS ACTIVE');
        $(itemIDStr).css('background-color', HEX_GREEN);
        
        $(`${itemIDStr} p.card-text`).html(`Time since detection: ${hoursDiff} hrs ${minsDiff} mins`);
      } else {
        console.log('IS NOT ACTIVE');
        $(itemIDStr).css('background-color', HEX_RED);
        $(`${itemIDStr} p.card-text`).html('Time since detection: N/A hrs N/A mins');
      }
    } else {
      console.log('DOES NOT EXIST');
      let rowNum = 0;
      let cardColor = isActive ? HEX_GREEN : HEX_RED;
      
      if (numAnomalies % 3 == 0) {
        $('div.price-channel-bot-item-container').append(`<div class="row price-channel-bot-item-row-${numAnomalies / 3}"></div>`);
        rowNum = numAnomalies / 3;
      } else {
        rowNum = Math.floor(numAnomalies / 3);
      }
      
      $(`div.price-channel-bot-item-row-${rowNum}`).append(`<div class="col-sm-4 card price-channel-bot-anomaly-item"><div id="${exchangeName}-${symbolName}" class="card-body" style="background-color:${cardColor};"><h3 class="card-title">${symbolNameHumanReadable}</h3><h5 class="card-subtitle">${exchangeNameHumanReadable}</h5><p class="card-text">Time since detection: ${hoursDiff} hrs ${minsDiff} mins</p></div></div>`);
      numAnomalies += 1;
    }
    
  } else {
    console.log(`ERROR: Data invalid ->\nExchange name: ${exchangeName}, symbol name: ${symbolName}, timestamp: ${timestamp}, is active: ${isActive}`);
  }
}

function isDataValid(_ExchangeName, _SymbolName, _AnomalyFirstDetected, _IsActive) {
  return _ExchangeName != null && _SymbolName != null &&
    _AnomalyFirstDetected != null && _IsActive != null;
}