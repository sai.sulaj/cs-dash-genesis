var HEX_GREEN = '#7cd991';
var HEX_RED = '#ea8e98';

if (!firebase.apps.length) {
  firebase.initalizeApp({});
}

var numAnomalies = 0;

$(document).ready(() => {
  initDataListeners();
});

function initDataListeners() {
  firebase.database().ref('marketSpreadBot').on('child_added', (exchangeSnapshot) => {
    console.log('Exchange added.');
    exchangeSnapshot.ref.on('child_added', (symbolSnapshot) => {
      console.log(`Exchange snapshot: ${exchangeSnapshot.numChildren()}`);
      console.log(`Symbol snapshot: ${symbolSnapshot.numChildren()}`);
      let timestampMin = Date.now();
      console.log(timestampMin);
      symbolSnapshot.child('timeseries').ref.orderByChild('timestamp').startAt(timestampMin).on('child_added', (spreadSnapshot) => {
        updateMarketSpreadBotAnomalies(exchangeSnapshot, symbolSnapshot, spreadSnapshot.child('spread').val());
      })
    });
  });
}

function updateMarketSpreadBotAnomalies(_ExchangeSnapshot, _SymbolSnapshot, _Spread) {
  let itemIDStr = `#${_ExchangeSnapshot.key}-${_SymbolSnapshot.key}`;
  let exists = $(itemIDStr).length == 1;
  
  let exchangeName = _ExchangeSnapshot.key;
  let symbolName = _SymbolSnapshot.key;
  
  if (isDataValid(exchangeName, symbolName, _Spread)) {
    
    let exchangeNameHumanReadable = exchangeName.toUpperCase().replace('_', '.');
    let symbolNameHumanReadable = symbolName.toUpperCase().replace('_', '/');
    let spreadPercent = _Spread * 100;
    let cardColor = spreadPercent > 1 ? HEX_GREEN : HEX_RED;
    
    if (exists) {
      console.log('EXISTS');
      $(`${itemIDStr} h3.card-title`).html(`${spreadPercent.toFixed(2)}%`);
      $(itemIDStr).css('background-color', cardColor);
    } else {
      console.log('DOES NOT EXIST');
      let rowNum = 0;
      
      if (numAnomalies % 3 == 0) {
        $('div.market-spread-bot-item-container').append(`<div class="row market-spread-bot-item-row-${numAnomalies / 3}"></div>`);
        rowNum = numAnomalies / 3;
      } else {
        rowNum = Math.floor(numAnomalies / 3);
      }
      
      $(`div.market-spread-bot-item-row-${rowNum}`).append(`<div class="col-sm-4 card market-spread-bot-anomaly-item"><div id="${exchangeName}-${symbolName}" class="card-body" style="background-color:${cardColor};"><h3 class="card-title">${spreadPercent.toFixed(2)}%</h3><h5 class="card-subtitle">${symbolNameHumanReadable}</h5><p class="card-text">${exchangeNameHumanReadable}</p></div></div>`);
      numAnomalies += 1;
    }
    
  } else {
    console.log(`ERROR: Data invalid ->\nExchange name: ${exchangeName}, symbol name: ${symbolName}, spread: ${spread}`);
  }
}

function isDataValid(_ExchangeName, _SymbolName, _Spread) {
  return _ExchangeName != null && _SymbolName != null &&
    _Spread != null;
}