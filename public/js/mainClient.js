// Initialize Firebase
var config = {
  apiKey: "AIzaSyBOjMd0-6sRam_gVxAb-ZNpdeIa07Xd3GY",
  authDomain: "cognisynth-dashboard.firebaseapp.com",
  databaseURL: "https://cognisynth-dashboard.firebaseio.com",
  projectId: "cognisynth-dashboard",
  storageBucket: "cognisynth-dashboard.appspot.com",
  messagingSenderId: "351090058844"
};
firebase.initializeApp(config);

var isSidebarExtended = false;

$(document).ready(() => {
  initSideBarExtendListener();
  initSideBarNavListener();
  
  $.get(window.location.protocol + '//' + window.location.host + '/api/earningsStraddleCalc', (html) => {
    $('div.main').html(html);
    $('#earnings-straddle-calculator-link').addClass('active');
  });
  
  $(document).keypress(triggerSubmit);
});

function triggerSubmit(event) {
  if (event.which == 13) {
    event.preventDefault();
    $('#button-get-data').trigger('click');
    $('#input-ticker-field').focus();
  }
}

function initSideBarExtendListener() {
  $('div.header button.sidebar-toggler').click(() => {
    if (isSidebarExtended) {
      closeSidebar();
    } else {
      openSidebar();
    }
  });
}

function openSidebar() {
  $('div.sidebar').css('width', '250px');
  $('div.header button.sidebar-toggler').css('margin-left', '250px');
  isSidebarExtended = true;
}

function closeSidebar() {
  $('div.sidebar').css('width', '0');
  $('div.header button.sidebar-toggler').css('margin-left', '0');
  isSidebarExtended = false;
}

function initSideBarNavListener() {
  $('#earnings-straddle-calculator-link').click(() => {
    $.get(window.location.protocol + '//' + window.location.host + '/api/earningsStraddleCalc', (html) => {
      $('div.main').html(html);
      $('div.sidebar a').removeClass('active');
      $('#earnings-straddle-calculator-link').addClass('active');
      $('div.header h3.nav-active-label').html('Earnings Straddle Calculator');
      closeSidebar();
    });
  });
  $('#price-channel-bot-link').click(() => {
    $.get(window.location.protocol + '//' + window.location.host + '/api/priceChannelBot', (html) => {
      $('div.main').html(html);
      $('div.sidebar a').removeClass('active');
      $('#price-channel-bot-link').addClass('active');
      $('div.header h3.nav-active-label').html('Price Channel Bot');
      closeSidebar();
    });
  });
  $('#market-spread-bot-link').click(() => {
    $.get(window.location.protocol + '//' + window.location.host + '/api/marketSpreadBot', (html) => {
      $('div.main').html(html);
      $('div.sidebar a').removeClass('active');
      $('#msb-link').addClass('active');
      $('div.header h3.nav-active-label').html('Market Spread Bot');
      closeSidebar();
    });
  });
}