var config = {
  apiKey: "AIzaSyBOjMd0-6sRam_gVxAb-ZNpdeIa07Xd3GY",
  authDomain: "cognisynth-dashboard.firebaseapp.com",
  databaseURL: "https://cognisynth-dashboard.firebaseio.com",
  projectId: "cognisynth-dashboard",
  storageBucket: "cognisynth-dashboard.appspot.com",
  messagingSenderId: "351090058844"
};
firebase.initializeApp(config);

var isLoginPartialVisible = true;

firebase.auth().onAuthStateChanged((_User) => {
  if (_User) {
    console.log(`User logged in: ${_User.email}`);
    window.location.href = '/main';
  } else {
    console.log('User signed out.');
  }
});

$(document).ready(() => {
  initSwitchAuthListener();
  initLoginAttemptListener();
});

$(document).keypress(function (e) {
  if (e.which == 13) {
    if (isLoginPartialVisible) {
      $('#button-login-submit').trigger('click');
    } else {
      $('#button-signup-submit').trigger('click');
    }
  }
});

function initLoginAttemptListener() {
  console.log('#initLoginAttemptListener');
  $('#button-login-submit').click(() => {
    console.log('#initLoginAttemptListener -> Click!');
    let email = $('#input-email-login-field').val();
    let password = $('#input-password-login-field').val();
    
    if (email == '') {
      alert('Please enter a valid email.');
    } else if (password == '') {
      alert('Please enter a valid password.');
    } else {
      login(email, password);
    }
  });
}

function initSignupAttemptListener() {
	$('#button-signup-submit').click(() => {
		var email = $('#email-field-signup').val();
		var password_1 = $('#password-field-signup').val();
		var password_2 = $('#password-2-field-signup').val();
		
		if (email == '') {
			alert('Please enter a valid email.');
		} else if (password_1 == '') {
			alert('Please enter a valid password.');
		} else if (password_2 != password_1) {
			alert('Please re-enter your password.');
		} else if (password_1.length < 6) {
			alert('Your password must contain atleast 6 characters.');
		} else {
			signup(email, password_1);
		}
	});
}

function initSwitchAuthListener() {
  $('#button-switch-auth').click(() => {
    if (isLoginPartialVisible) {
      $.get(window.location.protocol + '//' + window.location.host + '/api/signupPartial', (html) => {
        console.log(window.location.protocol + '//' + window.location.host + '/api/signupPartial');
        $('#auth-container').html(html);
        isLoginPartialVisible = false;
        $('#button-switch-auth').html("Login");
        initSignupAttemptListener();
      });
    } else {
      $.get(window.location.protocol + '//' + window.location.host + '/api/loginPartial', (html) => {
        $('#auth-container').html(html);
        isLoginPartialVisible = true;
        $('#button-switch-auth').html("Sign Up");
        initLoginAttemptListener();
      });
    }
  });
}

function login(_Email, _Password) {
  firebase.auth().signInWithEmailAndPassword(_Email, _Password)
    .catch((err) => {
    console.log(err);
    //TODO: Handle error.
  });
}

function signup(_Email, _Password) {
  firebase.auth().createUserWithEmailAndPassword(_Email, _Password)
    .catch((err) => {
    console.log(err);
    //TODO: Handle error.
  });
}