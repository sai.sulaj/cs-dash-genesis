var nextRow = 0;

var NO_DATA_ALERT = 0;
var DATE_IS_WEEKEND_ALERT = 1;
var INVALID_DATE_ALERT = 2;

const MONTH_INDICES = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];

var rowDateData = {};

$(document).ready(() => {
  initGetEarningsDataListener();
  initRemoveRowButtonListener();
  hideLoader();
  $('#input-ticker-field').focus();
});

function incrementNextRow () {
  if (nextRow > 500) {
    nextRow = 0;
  } else {
    nextRow += 1;
  }
}

function getEarningsReportDates(_Symbol, _Callback) {
  $.getJSON(`https://allorigins.me/get?url=https%3A//marketchameleon.com/symbols/earningsSimple%3Fs%3D${_Symbol}&callback=?`, function(data) {
    let parsedData = $('<div></div>');
    parsedData.html(data.contents);
    let parsedDates = [];
    $('#sym_earnings > tbody > tr', parsedData).each(function(index) {
      let pDate = $(this).children('td').first().text().trim().split('-');
      if (pDate.length > 1) { parsedDates.push(pDate); }
    });
    for (var i = 0; i < parsedDates.length; i++) {
      let year = parseInt(parsedDates[i][2]);
      let month = MONTH_INDICES.indexOf(parsedDates[i][1].toLowerCase());
      let day = parseInt(parsedDates[i][0].replace(',', ''));
      
      parsedDates[i] = new Date(year, month, day);
    }
    _Callback(parsedDates);
  });
}

function initGetEarningsDataListener() {
  $('#button-get-data').click(() => {
    showLoader()
    let symbol = $('#input-ticker-field').val();
    let year = $('#input-year-field').val();
    let month = $('#input-month-field').val();
    let day = $('#input-day-field').val();
    let dateString = `${year}-${month}-${day}`;
    let date = new Date(dateString);
    
    if (year === '') {
      getEarningsReportDates(symbol, (dates) => {
        for (var i = 0; i < dates.length; i++) {
          console.log(`DATES: ${dates[i]}`);
          $.get(window.location.protocol + '//' + window.location.host + '/earningsStraddleCalculator/getEarningsData', {
            symbol: symbol,
            date: dates[i].toISOString().slice(0, 10),
            index: i
          }, (_Data) => {
            if (_Data.hasOwnProperty('index')) {
              postData(_Data.data, dates[parseInt(_Data.index)]);
            }
            $('#input-ticker-field').focus();
          });
        }
      });
    } else {
      try {
        date.toISOString();
      } catch (err) {
        displayAlert(INVALID_DATE_ALERT);
        return;
      }
    
      $.get(window.location.protocol + '//' + window.location.host + '/earningsStraddleCalculator/getEarningsData', {
        symbol: symbol,
        date: date.toISOString().slice(0, 10)
      }, (_Data) => {
        console.log(_Data);
        postData(_Data.data, date);
        $('#input-ticker-field').focus();
      });
    }
  });
}

function updateAvgVals () {
  let avgPreERDelta = 0;
  let avgPreERDeltaPerc = 0;
  let avgERDelta = 0;
  let avgERDeltaPerc = 0;
  let avgPostERDelta = 0;
  let avgPostERDeltaPerc = 0;
  let avgTotDelta = 0;
  let avgTotDeltaPerc = 0;
  let totVals = $('tr.tr-data-row').length;
  
  $('tr.tr-data-row').each(function() {
    $this = $(this);
    avgPreERDelta += Math.abs(parseFloat($this.find('td.td-pre-er-delta').html()));
    avgPreERDeltaPerc += Math.abs(parseFloat($this.find('td.td-pre-er-delta-perc').html()));
    avgERDelta += Math.abs(parseFloat($this.find('td.td-er-delta').html()));
    avgERDeltaPerc += Math.abs(parseFloat($this.find('td.td-er-delta-perc').html()));
    avgPostERDelta += Math.abs(parseFloat($this.find('td.td-post-er-delta').html()));
    avgPostERDeltaPerc += Math.abs(parseFloat($this.find('td.td-post-er-delta-perc').html()));
    avgTotDelta += Math.abs(parseFloat($this.find('td.td-tot-delta').html()));
    avgTotDeltaPerc += Math.abs(parseFloat($this.find('td.td-tot-delta-perc').html()));
  });
  
  console.log(`${avgPreERDelta}, ${avgPreERDeltaPerc}, ${avgERDelta}, ${avgERDeltaPerc}, ${avgPostERDelta}, ${avgPostERDeltaPerc}, ${avgTotDelta}, ${avgTotDeltaPerc}, ${totVals}, `);
  
  $('#pre-er-delta').html((avgPreERDelta / totVals).toFixed(2));
  $('#pre-er-delta-perc').html((avgPreERDeltaPerc / totVals).toFixed(2));
  $('#er-delta').html((avgERDelta / totVals).toFixed(2));
  $('#er-delta-perc').html((avgERDeltaPerc / totVals).toFixed(2));
  $('#post-er-delta').html((avgPostERDelta / totVals).toFixed(2));
  $('#post-er-delta-perc').html((avgPostERDeltaPerc / totVals).toFixed(2));
  $('#tot-avg-delta').html((avgTotDelta / totVals).toFixed(2));
  $('#tot-avg-delta-perc').html((avgTotDeltaPerc / totVals).toFixed(2));
}

function initRemoveRowButtonListener(_Num) {
  $(`#button-remove-row-${_Num}`).click(() => {
    console.log('click');
    $(`#tr-data-row-${_Num}`).remove();
    delete rowDateData[_Num];
    console.log(rowDateData);
    $(".tooltip").tooltip("hide");
    updateAvgVals();
    //colorRows();
  });
}

function postData(_Data, _Date) {
  if (_Data == null || _Data == undefined) {
    displayAlert(NO_DATA_ALERT);
    return;
  }
  
  let dates = getDateArray(_Date, _Data);
  
  if (dates == INVALID_DATE_ALERT) {
    displayAlert(INVALID_DATE_ALERT);
    return;
  } else if (dates == DATE_IS_WEEKEND_ALERT) {
    displayAlert(DATE_IS_WEEKEND_ALERT);
    return;
  }
  
  try {
    let firstClose = _Data.filter((item) => { return parseISOString(item.date).toDateString() == dates[0].toDateString() })[0].close;
    let preERClose = _Data.filter((item) => { return parseISOString(item.date).toDateString() == dates[1].toDateString() })[0].close;
    let ERClose = _Data.filter((item) => { return parseISOString(item.date).toDateString() == dates[2].toDateString() })[0].close;
    let lastClose = _Data.filter((item) => { return parseISOString(item.date).toDateString() == dates[3].toDateString() })[0].close;
    
    console.log(`FIRST: ${firstClose}, PRE ER: ${preERClose}, ER: ${ERClose}, LAST: ${lastClose}`);
  
    let preERDelta = preERClose - firstClose;
    let preERDeltaPerc = preERDelta / firstClose * 100;
    let ERDelta = ERClose - preERClose;
    let ERDeltaPerc = ERDelta / preERClose * 100;
    let postERDelta = lastClose - ERClose;
    let postERDeltaPerc = postERDelta / ERClose * 100;
    let totalDelta = lastClose - firstClose;
    let totalDeltaPerc = totalDelta / lastClose * 100;
  
    console.log(`${preERDelta}, ${preERDeltaPerc}, ${ERDelta}, ${ERDeltaPerc}, ${postERDelta}, ${postERDeltaPerc}`);
  
    $('#tbody-ticker-data').append(`<tr class="tr-data-row" id="tr-data-row-${nextRow}" data-toggle="tooltip" title="Earliest Date: ${dates[0].toDateString()} Close Price: $${firstClose.toFixed(2)}\nPre-ER Date: ${dates[1].toDateString()} Close Price: $${preERClose.toFixed(2)}\nER Date: ${dates[2].toDateString()} Close Price: $${ERClose.toFixed(2)}\nLatest Date: ${dates[3].toDateString()} Close Price: $${lastClose.toFixed(2)}"><td class="td-date">${_Date.toDateString()}</td><td class="td-pre-er-delta">${preERDelta.toFixed(2)}</td><td class="td-pre-er-delta-perc">${preERDeltaPerc.toFixed(2)}</td><td class="td-er-delta">${ERDelta.toFixed(2)}</td><td class="td-er-delta-perc">${ERDeltaPerc.toFixed(2)}</td><td class="td-post-er-delta">${postERDelta.toFixed(2)}</td><td class="td-post-er-delta-perc">${postERDeltaPerc.toFixed(2)}</td><td class="td-tot-delta">${totalDelta.toFixed(2)}</td><td class="td-tot-delta-perc">${totalDeltaPerc.toFixed(2)}</td><td class="td-remove"><button id="button-remove-row-${nextRow}" class="btn btn-danger">X</button></td></tr>`);
    initRemoveRowButtonListener(nextRow);
    $(`#tr-data-row-${nextRow}`).tooltip();
    rowDateData[nextRow] = dates;
    incrementNextRow();
    updateAvgVals();
    //colorRows();
    hideLoader();
  } catch (err) {
    console.log(err);
    displayAlert(NO_DATA_ALERT);
    return;
  }
}

function showLoader() {
  $('#status').find('h4').text('LOADING...');
  $('#status').show();
}

function hideLoader() {
  $('#status').hide();
}

function displayAlert(_Type) {
  switch (_Type) {
    case NO_DATA_ALERT:
      $('#status').find('h4').text('NO DATA FOUND');
      break;
    case DATE_IS_WEEKEND_ALERT:
      $('#status').find('h4').text('DATE IS WEEKEND');
      break;
    case INVALID_DATE_ALERT:
      $('#status').find('h4').text('INVALID DATE');
      break;
  }
}

function getDateArray(_Date, _Data) {
  if (_Date.getDay() < 1 || _Date.getDay() > 5) { return DATE_IS_WEEKEND_ALERT; }
  let datesArray = [null,null,_Date,null, false];
  
  let preERDate = new Date(_Date);
  preERDate.setDate(preERDate.getDate() - 1);
  
  let preERDateArray = _Data.filter((item) => {
    return parseISOString(item.date).toDateString() == preERDate.toDateString(); 
  });
  if (preERDateArray.length == 0) { datesArray[4] = true; }
  var iterations = 0;
  while (preERDateArray.length == 0) {
    if (iterations > 100) { console.log('Nah'); return -1; }
    
    preERDate.setDate(preERDate.getDate() - 1);
    preERDateArray = _Data.filter((item) => {
      return parseISOString(item.date).toDateString() == preERDate.toDateString(); 
    });
    iterations += 1;
  }
  datesArray[1] = parseISOString(preERDateArray[0].date);
  
  datesArray[0] = parseISOString(_Data[_Data.length - 1].date);
  datesArray[3] = parseISOString(_Data[0].date);
  return datesArray;
}

function parseISOString(_Str) {
  let dateArray = _Str.slice(0, 10).split('-');
  for (var i = 0; i < dateArray.length; i++) { dateArray[i] = parseInt(dateArray[i]); }
  return new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
}

function colorRows() {
  $('tr.tr-data-row').each((i, el) => {
    if (i % 2 == 0) {
      $(el).css('background-color', '#f5f5f5');
    } else {
      $(el).css('background-color', '#ffffff');
    }
  });
}