/**
 * Author: Saimir Sulaj.
 * Date: March 25, 2018.
 * Purpose: High-level console for all things involving Cognisynth Capital.
 */

// --------------- DEPENDENCIES --------------- //

const express                           = require('express');
const bodyParser                        = require('body-parser');
const config                            = require('config');

const router                            = require('./router.js');
const EarningsStraddleCalculatorRouter  = require('./compModules/EarningsStraddleCalculator/router.js');
const PriceChannelBotRouter             = require('./compModules/PriceChannelBot/router.js');
const MarketSpreadBotRouter             = require('./compModules/MarketSpreadBot/router.js');

const app                   = express();

// --------------- GLOBAL VARS & CONSTANTS --------------- //

const DEBUG                 = config.get('debug');
const TESTING               = config.get('testing');

// --------------- ROUTING CONFIG --------------- //

app.set('view engine', 'ejs');
app.set('view cache', !TESTING);
app.use('/public', express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', router);
app.use('/earningsStraddleCalculator', EarningsStraddleCalculatorRouter);
//app.use('/priceChannelBot', PriceChannelBotRouter); // TODO: Uncomment when adding pcb api.
//app.use('/marketSpreadBot', MarketSpreadBotRouter);

let port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});