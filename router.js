/**
 * Author: Saimir Sulaj.
 * Date: March 25, 2018.
 * Purpose: Express route functions.
 */

// --------------- DEPEDENCIS --------------- //

const express              = require('express');

const router               = express.Router();

// --------------- ROUTES --------------- //

router.get('/', (req, res) => {
  res.render('login');
});

router.get('/api/signupPartial', (req, res) => {
  res.render('signupPartial');
});

router.get('/api/loginPartial', (req, res) => {
  res.render('loginPartial');
});

router.get('/api/earningsStraddleCalc', (req, res) => {
  res.render('EarningsStraddleCalculator/earningsStraddleCalculatorView');
})

router.get('/api/priceChannelBot', (req, res) => {
  res.render('PriceChannelBot/priceChannelBotView');
})

router.get('/api/marketSpreadBot', (req, res) => {
  res.render('MarketSpreadBot/marketSpreadBotDashboard');
})

router.get('/main', (req, res) => {
  res.render('main');
});

module.exports = router;